module "vpc"{
    source = "../../modules/network"
    vpc_cidr_block = var.vpc_cidr_block
    dns_enable = var.dns_enable
    public_cidrs = var.public_cidrs
    private_cidrs = var.private_cidrs
    azs = var.azs
    public_ip_map = var.public_ip_map
    private_ip_map = var.private_ip_map
    env = var.env
}