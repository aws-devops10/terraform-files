module "vpc"{
    source = "../../modules/network"
    vpc_cidr_block = var.vpc_cidr_block
    dns_enable = var.dns_enable
    public_cidrs = var.public_cidrs
    azs = var.azs
    public_ip_map = var.public_ip_map
    env = var.env
}