region = "us-east-1"
vpc_cidr_block = "10.1.0.0/16"
dns_enable = true
public_cidrs = ["10.1.1.0/24","10.1.2.0/24"]
azs = ["us-east-1a", "us-east-1b", "us-east-1c"]
public_ip_map = true
env = "staging"
