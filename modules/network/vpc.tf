####################################### 
#############   vpc ##################
#######################################
resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr_block
  enable_dns_hostnames = var.dns_enable

    tags = {
    Name = "${var.env}-vpc"
  }
}
####################################### 
#######   Public_Subnets ############
#######################################
resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.main.id
  cidr_block   = var.public_cidrs[count.index]
  count        = length(var.public_cidrs)
  availability_zone       = element(var.azs, count.index)
  map_public_ip_on_launch = var.public_ip_map
 
  tags = {
    Name = "${var.env}-public_subnet-${count.index + 1}"
  }
}
####################################### 
#######   Private_Subnets ############
#######################################
resource "aws_subnet" "private" {
  vpc_id     = aws_vpc.main.id
  cidr_block   = var.private_cidrs[count.index]
  count        = length(var.private_cidrs)
  availability_zone       = element(var.azs, count.index)
  map_public_ip_on_launch = var.private_ip_map
 
  tags = {
    Name = "${var.env}-private_subnet-${count.index + 1}"
  }
}

